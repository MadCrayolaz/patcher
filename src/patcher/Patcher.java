package patcher;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Patcher {

    private Gson gson = new Gson();
    private Version vers;
    private Version tempVers;

    private static String myDocuments;

    private File cVersion = null;
    private File cTempVersion = null;
    private File cJar = null;
    private File cTempJar = null;
    private File cIPPorts = null;
    private File cTempIPPorts = null;

    public Patcher() {
    }

    public void checkVersion(String myDocs) {
        cVersion = new File(myDocs + "\\PetuniaGUI\\version.txt");
        cTempVersion = new File(myDocs + "\\PetuniaGUI\\temp\\version.txt");
        URL url = null;
        //Check For Valid URL
        try {
            url = new URL("http://petuniaserver.com/launchconfig/version.txt");
        }
        catch(MalformedURLException ex) {
            System.out.println("URL Is Wrong/Missing!");
        }
        downloadFile(url, cTempVersion);

        if(fileExists(cVersion)) {
            try {
                vers = gson.fromJson(readFile(myDocs + "\\PetuniaGUI\\version.txt", Charset.defaultCharset()), Version.class);
                tempVers = gson.fromJson(readFile(myDocs + "\\PetuniaGUI\\temp\\version.txt", Charset.defaultCharset()), Version.class);
            }
            catch(IOException ex) {
                Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(getVersion().equals(getTempVersion())) {
                deleteFile(cTempVersion);
                downloadIPPorts(myDocs);
                return;
            }
            else {
                deleteFile(cVersion);
                moveFile(cTempVersion, cVersion);
                replaceJar(myDocs);
            }
        }
        else {
            moveFile(cTempVersion, cVersion);
            replaceJar(myDocs);
        }
    }

    public void replaceJar(String myDocs) {
        cJar = new File(myDocs + "\\PetuniaGUI\\TempGUI.jar");
        cTempJar = new File(myDocs + "\\PetuniaGUI\\temp\\TempGUI.jar");
        URL url = null;
        //Check For Valid URL
        try {
            url = new URL("http://www.petuniaserver.com/launchconfig/TempGUI.jar");
        }
        catch(MalformedURLException ex) {
            System.out.println("URL Is Wrong/Missing!");
        }

        if(fileExists(cJar)) {
            deleteFile(cJar);
        }
        downloadFile(url, cTempJar);
        moveFile(cTempJar, cJar);
        downloadIPPorts(myDocs);
    }

    public void downloadIPPorts(String myDocs) {
        cIPPorts = new File(myDocs + "\\PetuniaGUI\\ipports.txt");
        cTempIPPorts = new File(myDocs + "\\PetuniaGUI\\temp\\ipports.txt");
        URL url = null;
        //Check For Valid URL
        try {
            url = new URL("http://petuniaserver.com/launchconfig/ipports.txt");
        }
        catch(MalformedURLException ex) {
            System.out.println("URL Is Wrong/Missing!");
        }
        
        if(fileExists(cIPPorts)) {
            deleteFile(cIPPorts);
        }
        downloadFile(url, cTempIPPorts);
        moveFile(cTempIPPorts, cIPPorts);
    }

    public String getVersion() {
        return vers.version;
    }

    public String getTempVersion() {
        return tempVers.version;
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public boolean fileExists(File path) {
        return path.exists();
    }

    public void downloadFile(URL url, File dir) {
        if(url == null) {
            System.out.println("URL Was Not Passed!");
            return;
        }
        URL website = url;
        try {
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(dir);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        }
        catch(IOException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteFile(File dir) {
        Path path = Paths.get(dir.getAbsolutePath());
        try {
            Files.delete(path);
        }
        catch(IOException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void moveFile(File source, File target) {
        Path path1 = Paths.get(source.getAbsolutePath());
        Path path2 = Paths.get(target.getAbsolutePath());
        try {
            Files.move(path1, path2, REPLACE_EXISTING);
        }
        catch(IOException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getMyDocs() {
        try {
            Process p = Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
            p.waitFor();

            InputStream in = p.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();

            myDocuments = new String(b);
            myDocuments = myDocuments.split("\\s\\s+")[4];

        }
        catch(Throwable t) {
            t.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Patcher p = new Patcher();
        p.getMyDocs();
        p.checkVersion(myDocuments);
        System.out.println(myDocuments);

        String[] cmd = {"java", "-jar", "TempGUI.jar"};
        try {
            Runtime.getRuntime().exec(cmd);
        }
        catch(IOException ex) {
            Logger.getLogger(Patcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}